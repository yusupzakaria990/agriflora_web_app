import "./App.css";
import Header from "./components/Header";
import HomePage from "./components/HomePage";
import Footer from "./components/Footer";
import AboutUs from "./components/AboutUs";
import Product from "./components/Product";
import Register from "./components/Register";
import Contact from "./components/Contact";
import LoginPage from "./components/LoginPage";
import NotFound from "./components/NotFound";
import Dashboard from "./dashboard/Dashboard";
import { Routes, Route } from "react-router-dom";
import { ChakraProvider, ColorModeScript  } from "@chakra-ui/react";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


function App() {
  return (
    <ChakraProvider>
      <ColorModeScript initialColorMode="light" />
      <main>
        <Routes>
          <Route
            path="/"
            element={
              <>
                <Header />
                <HomePage />
                <Footer />
              </>
            }
          />
          <Route path="login" element={<LoginPage />} />
          <Route
            path="about"
            element={
              <>
                <Header />
                <AboutUs />
                <Footer />
              </>
            }
          />
          <Route
            path="product"
            element={
              <>
                <Header />
                <Product />
                <Footer />
              </>
            }
          />
          <Route
            path="contact"
            element={
              <>
                <Header />
                <Contact />
                <Footer />
              </>
            }
          />
          <Route
            path="register"
            element={
              <>
                <Register />
              </>
            }
          />
          <Route
            path="notfound"
            element={
              <>
                <NotFound />
              </>
            }
          />
          <Route
            path="dashboard"
            element={
              <>
                <Dashboard />
              </>
            }
          />
        </Routes>
        <ToastContainer />
      </main>
    </ChakraProvider>
  );
}

export default App;
