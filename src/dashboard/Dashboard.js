import NotFound from "../components/NotFound";
import Footer from "../components/Footer";
import Navbar from "./Navbar";
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import CaptionCarousel from "../components/dashboard/CaptionCarousel";

const Dashboard = () => {
    const navigate = useNavigate();
    useEffect(() => {
        const token = localStorage.getItem('token');
        if (!token) {
        navigate('/login');
        }
      }, [navigate]);

    useEffect(() => {
    // Mencegah pengguna kembali ke halaman sebelumnya dengan mengganti riwayat penjelajah
    window.history.pushState(null, null, '/dashboard');
}, []);
    return(
        <>
        <Navbar />
        <CaptionCarousel/>
        <NotFound />
        <Footer />
      </>
    );
};

export default Dashboard;