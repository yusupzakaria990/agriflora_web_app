import {
  Container,
  Box,
  Avatar,
  Button,
  HStack,
  VStack,
  Image,
  Input,
  Spacer,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  Text,
  Link,
  MenuDivider,
  useColorModeValue
} from '@chakra-ui/react';
import { useNavigate } from "react-router-dom";
import { ReactNode } from 'react';
import ColorModeToggle from '../components/ColorModeToggle';
import { useColorMode } from '@chakra-ui/react';

type IconButtonProps = {
  children: ReactNode;
};


const IconButton = ({ children }: IconButtonProps) => {
  return (
    <Button
      padding="0.4rem"
      width="auto"
      height="auto"
      borderRadius="100%"
      bg="transparent"
      _hover={{ bg: '#f6f6f6' }}
    >
      {children}
    </Button>
  );
};

const Navbar = () => {
  const { colorMode } = useColorMode();
  const navigate = useNavigate();

  const handleLogout = () => {
    localStorage.removeItem('token', 'user_id');
    navigate("/login");
  };

  return (
    <Box
      py="2"
      boxShadow="lg"
      border="0 solid #e5e7eb"
      position="fixed"
      top="0"
      width="100%"
      zIndex="1"
    >
      <Container maxW="1280px" px={4} mx="auto">
        <HStack spacing={4}>
          <Image
            alt="dev logo"
            w={'auto'}
            h={12}
            src="agriflora_logo.png"
          />
          <Input
            maxW="26rem"
            placeholder="Search..."
            borderColor={useColorModeValue('gray.300', 'white')}
            borderRadius="5px"
            d={{ base: 'none', md: 'block' }}
          />
          <Spacer />
          <HStack spacing={3}>
            <IconButton>
              <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 576 512">
                <path d="M0 24C0 10.7 10.7 0 24 0H69.5c22 0 41.5 12.8 50.6 32h411c26.3 0 45.5 25 38.6 50.4l-41 152.3c-8.5 31.4-37 53.3-69.5 53.3H170.7l5.4 28.5c2.2 11.3 12.1 19.5 23.6 19.5H488c13.3 0 24 10.7 24 24s-10.7 24-24 24H199.7c-34.6 0-64.3-24.6-70.7-58.5L77.4 54.5c-.7-3.8-4-6.5-7.9-6.5H24C10.7 48 0 37.3 0 24zM128 464a48 48 0 1 1 96 0 48 48 0 1 1 -96 0zm336-48a48 48 0 1 1 0 96 48 48 0 1 1 0-96z"
                fill={colorMode === 'dark' ? 'white' : 'black'}
                />
              </svg>
            </IconButton>
            <IconButton>
              <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512">
                <path d="M224 0c-17.7 0-32 14.3-32 32V51.2C119 66 64 130.6 64 208v18.8c0 47-17.3 92.4-48.5 127.6l-7.4 8.3c-8.4 9.4-10.4 22.9-5.3 34.4S19.4 416 32 416H416c12.6 0 24-7.4 29.2-18.9s3.1-25-5.3-34.4l-7.4-8.3C401.3 319.2 384 273.9 384 226.8V208c0-77.4-55-142-128-156.8V32c0-17.7-14.3-32-32-32zm45.3 493.3c12-12 18.7-28.3 18.7-45.3H224 160c0 17 6.7 33.3 18.7 45.3s28.3 18.7 45.3 18.7s33.3-6.7 45.3-18.7z"
                fill={colorMode === 'dark' ? 'white' : 'black'}
                />
              </svg>
            </IconButton>
            <Menu isLazy>
              <MenuButton as={Button} size="sm" px={0} py={0} rounded="full">
                <Avatar size="sm" src={'https://avatars2.githubusercontent.com/u/37842853?v=4'} />
              </MenuButton>
              <MenuList
                zIndex={5}
                border="2px solid"
                borderColor={useColorModeValue('gray.700', 'gray.100')}
                boxShadow="4px 4px 0"
              >
                <Link href="https://dev.to/m_ahmad" _hover={{ textDecoration: 'none' }} isExternal>
                  <MenuItem>
                    <VStack justify="start" alignItems="left">
                      <Text fontWeight="500">Roy Kimochi</Text>
                      <Text size="sm" color="gray.500" mt="0 !important">
                        @roy
                      </Text>
                    </VStack>
                  </MenuItem>
                </Link>
                <MenuDivider />
                <MenuItem>
                  <Text fontWeight="500">Settings</Text>
                </MenuItem>
                <MenuDivider />
                <MenuItem onClick={handleLogout}>
                  <Text fontWeight="500">Sign Out</Text>
                </MenuItem>
              </MenuList>
              <ColorModeToggle />
            </Menu>
          </HStack>
        </HStack>
      </Container>
    </Box>
  );
};

export default Navbar;