import axios from 'axios';

export const registerUser = async (userData) => {
    try {
      const response = await axios.post('http://localhost:3030/user/register', userData);
      return response.data;
    } catch (error) {
      throw error;
    }
  };