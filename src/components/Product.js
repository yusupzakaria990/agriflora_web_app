import React from "react";
import "./HomePage.css";
import {
    Box,
    Text,
    Image,
    Heading,
    Grid,
    GridItem,
    Flex,
    Link,
    Center,

} from "@chakra-ui/react";
import PlantCard from "./PlantCard";

const Product = () => {
    const plants = [
        { id: 4, name: "Herbamin", price: 25, imageSrc: "herb.jpeg" },
        { id: 5, name: "Treas", price: 25, imageSrc: "treas.jpeg" },
        { id: 6, name: "Greneery", price: 25, imageSrc: "Greenery.jpeg" },
        { id: 7, name: "Monstera", price: 25, imageSrc: "Monstera_Deliciosa.jpeg" },
    ];
    return (
        <div className="body-container">

            <Heading
                as="h4"
                size="md"
                textAlign="center"
                paddingTop={0}
                paddingBottom={10}
            >
                Get By Categories
            </Heading>

            <Grid templateColumns="repeat(4, 1fr)" gap={4}>
                <GridItem>
                    <Box textAlign="center">
                        <Image
                            src="indoor.jpeg"
                            alt="Gambar 1"
                            width="100%"
                            height="auto"
                        />
                        <Text>Indoor Plant</Text>
                    </Box>
                </GridItem>
                <GridItem>
                    <Box textAlign="center">
                        <Image
                            src="outdoor.jpeg"
                            alt="Gambar 2"
                            width="100%"
                            height="auto"
                        />
                        <Text>Outdoor Plant</Text>
                    </Box>
                </GridItem>
                <GridItem>
                    <Box textAlign="center">
                        <Image src="herb.jpeg" alt="Gambar 3" width="100%" height="auto" />
                        <Text>Herb</Text>
                    </Box>
                </GridItem>
                <GridItem>
                    <Box textAlign="center">
                        <Image
                            src="cactus.jpeg"
                            alt="Gambar 4"
                            width="100%"
                            height="auto"
                        />
                        <Text>Cactus</Text>
                    </Box>
                </GridItem>
            </Grid>
            <Flex direction="column" align="center" color="black" py={3}>
                <Box as="header">
                    <Heading as="h4" size="md" textAlign="center" pt={20} pb={10}>
                        Our Product
                    </Heading>
                </Box>
                <Box as="nav">
                    <Center>
                        <Link href="#" mr={32}>
                            Indoor Plant
                        </Link>
                        <Link href="#" mr={32}>
                            Outdoor Plant
                        </Link>
                        <Link href="#" mr={32}>
                            Seed
                        </Link>
                        <Link href="#">Stuf</Link>
                    </Center>
                </Box>
            </Flex>
            <Flex justifyContent="space-between">
                {plants.map((plant) => (
                    <PlantCard
                        key={plant.id}
                        name={plant.name}
                        price={plant.price}
                        imageSrc={plant.imageSrc}
                    />
                ))}
            </Flex>
        </div>
    );
};

export default Product;