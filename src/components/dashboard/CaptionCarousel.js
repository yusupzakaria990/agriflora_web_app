import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const CaptionCarousel = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };

    const carouselContainerStyle = {
        marginTop: '15vh',
        marginLeft:'7vh',
        marginRight:'7vh',
    };


    return (
        <div style={carouselContainerStyle}>
            <Slider {...settings}>
                <div>
                    <img src="6974515.jpg" alt="Gambar 1" />
                </div>
                <div>
                    <img src="6974459.jpg" alt="Gambar 2" />
                </div>
            </Slider>
        </div>
    );
};

export default CaptionCarousel;
