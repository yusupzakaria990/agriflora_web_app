import { Box, Grid, Heading, Text, Icon, Link } from "@chakra-ui/react";
import { FaFacebook, FaTwitter, FaInstagram } from "react-icons/fa";

function Footer() {
  return (
    <Box bg="teal" color="white" py={8}>
      <Grid
        templateColumns="repeat(4, 1fr)"
        gap={4}
        maxW="1200px"
        margin="0 auto"
      >
        <Box>
          <Heading as="h5" size="sm" mb={2}>
            Agriflora
          </Heading>
          <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>
        </Box>
        <Box>
          <Heading as="h5" size="sm" mb={2}>
            About Us
          </Heading>
          <Text>
            Nulla vehicula odio auctor, commodo massa quis, bibendum magna.
          </Text>
        </Box>
        <Box>
          <Heading as="h5" size="sm" mb={2}>
            Contact
          </Heading>
          <Text>
            Email: info@example.com
            <br />
            Phone: +123456789
          </Text>
        </Box>
        <Box>
          <Heading as="h5" size="sm" mb={2}>
            Social Media
          </Heading>
          <Link href="#">
            <Icon as={FaFacebook} boxSize={6} mr={2} />
          </Link>
          <Link href="#">
            <Icon as={FaTwitter} boxSize={6} mr={2} />
          </Link>
          <Link href="#">
            <Icon as={FaInstagram} boxSize={6} />
          </Link>
        </Box>
      </Grid>
    </Box>
  );
}

export default Footer;
