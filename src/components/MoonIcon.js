import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMoon } from '@fortawesome/free-regular-svg-icons';

const MoonIcon = () => <FontAwesomeIcon icon={faMoon} size="lg" />;

export default MoonIcon;
