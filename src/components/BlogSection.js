import { Grid, Box, Text, Button } from "@chakra-ui/react";

function BlogSection() {
  return (
    <Grid templateColumns="1fr 1fr" gap={4}>
      <Box>
        <Text fontSize="lg">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec
          fermentum enim. Duis sagittis sed arcu eu varius.
        </Text>
      </Box>
      <Box>
        <Button colorScheme="teal" size="sm">
          See All
        </Button>
      </Box>
    </Grid>
  );
}

export default BlogSection;
