import React, { useState, useEffect } from "react";
import { registerUser } from "../api/authentication";
import { useNavigate } from "react-router-dom";
import { useColorMode } from '@chakra-ui/react';

import {
  Box,
  Flex,
  Image,
  Input,
  Button,
  FormControl,
  FormLabel,
  VStack,
  Text,
} from "@chakra-ui/react";
import { toast } from "react-toastify";

const Register = () => {
  const { setColorMode } = useColorMode();
  useEffect(() => {
    setColorMode('light');
  }, [setColorMode]);
  
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    username: "",
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const { id, value } = e.target;
    setFormData({
      ...formData,
      [id]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await registerUser(formData);
      console.log("Registrasi berhasil!", response);

      navigate("/login");
      toast.success("Your Account has been Registered");

    } catch (error) {
      if (error.response && error.response.status === 400) {
        toast.error("Email already taken");
      }else if(error.response.status === 401){
        const { errors } = error.response.data;
        if (errors && Array.isArray(errors) && errors.length > 0) {
          // Ambil dari array pertama
          const firstError = errors[0];
          toast.error(firstError.msg);
        };
      }
      else {
        toast.error("Have an Error");
      }
    }
  };

  return (
    <Flex minH="100vh" align="center" justify="center" bg="gray.100">
      <Box
        borderWidth={1}
        px={20}
        py={8}
        bg="white"
        borderRadius="md"
        boxShadow="md"
      >
        <Flex align="center" justify="space-between">
          <Box mr={4}>
            <Image src="/garden2.png" alt="Gambar" width={450} />
          </Box>

          <VStack spacing={4}>
            <Image
              src="agriflora_logo.png"
              w="200px"
              h="auto"
              alt="Produk Agriflora"
            />
            <Text fontSize="sm">
              Lets connect to our site and join with us.
            </Text>
            return (
            <form onSubmit={handleSubmit}>
              <FormControl id="username" isRequired>
                <FormLabel>Username</FormLabel>
                <Input
                  type="text"
                  placeholder="Username"
                  id="username"
                  value={formData.username}
                  onChange={handleChange}
                />
              </FormControl>
              <FormControl id="email" isRequired marginTop={3}>
                <FormLabel>Email</FormLabel>
                <Input
                  type="email"
                  placeholder="Email"
                  id="email"
                  value={formData.email}
                  onChange={handleChange}
                />
              </FormControl>
              <FormControl id="password" isRequired marginTop={3}>
                <FormLabel>Password</FormLabel>
                <Input
                  type="password"
                  placeholder="Password"
                  id="password"
                  value={formData.password}
                  onChange={handleChange}
                />
              </FormControl>
              <Button
                marginTop={3}
                colorScheme="teal"
                type="submit"
                borderRadius={20}
              >
                Sign Up
              </Button>
            </form>
            );
            <Text fontSize="sm">
              Have An Account?{" "}
              <a href="/login" style={{ color: "blue" }}>
                Login Here!
              </a>
            </Text>
          </VStack>
        </Flex>
      </Box>
    </Flex>
  );
};

export default Register;
