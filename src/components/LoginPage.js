import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import axios from 'axios';
import { useNavigate } from "react-router-dom";
import { useColorMode } from '@chakra-ui/react';
import {
  Box,
  Flex,
  Image,
  Input,
  Button,
  FormControl,
  FormLabel,
  VStack,
  Text,
} from "@chakra-ui/react";

const LoginPage = () => {
  const { setColorMode } = useColorMode();
  useEffect(() => {
    setColorMode('light');
  }, [setColorMode]);


  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post('http://localhost:3030/user/login', {
        email: email,
        password: password,
      });

      // Handle the API response
      const { user_id, token, message } = response.data;
      if (message === 'Authentication Success') {
        console.log('User ID:', user_id);
        console.log('Token:', token);

        localStorage.setItem('token', response.data.token);
        localStorage.setItem('user_id', response.data.user_id);
        window.history.replaceState(null, null, "/dashboard");
        navigate("/dashboard");

        toast.success("Login Successfull")

      } else {
        toast.error('Authentication failed. Please check your credentials.')
      }
    } catch (error) {
      console.error('Error:', error);
      console.log(error.response.data.message);
      if(error.response.status === 401){
        toast.error(error.response.data.message);

      }else{
        toast.error("An error occurred during login. Please try again later");
      };
    }
  };

  return (
    <Flex
      minH="100vh"
      align="center"
      justify="center"
      bg="gray.100"
    >
      <Box
        borderWidth={1}
        px={20}
        py={8}
        bg="white"
        borderRadius="md"
        boxShadow="md"
      >
        <Flex align="center" justify="space-between">
          <Box mr={4}>
            <Image src="/garden2.png" alt="Gambar" width={450} />
          </Box>

          <VStack spacing={4}>
            <Image src="agriflora_logo.png" w="200px" h="auto" alt="Produk Agriflora" />
            <Text fontSize="sm">
              Connect to your account
            </Text>
            return (
            <form onSubmit={handleLogin}>
              <FormControl id="email" isRequired>
                <FormLabel>Email</FormLabel>
                <Input
                  type="email"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </FormControl>
              <FormControl id="password" isRequired marginTop={3}>
                <FormLabel>Password</FormLabel>
                <Input
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </FormControl>
              <Button colorScheme="teal" type="submit" borderRadius={20} marginTop={3}>
                Sign In
              </Button>
            </form>
            );
            <Text fontSize="sm">
              Doesn't Have Account? <a href="/Register" style={{ color: "blue" }}>Register Here!</a>
            </Text>
          </VStack>
        </Flex>
      </Box>
    </Flex>
  );
};

export default LoginPage;
