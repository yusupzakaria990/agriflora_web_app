import React from "react";
import "./HomePage.css";
import {
  Box,
  Flex,
  Text,
  Image,
  Button,
  Stack,
  Heading,
  Grid,
  GridItem,
  Center,
  Link,
} from "@chakra-ui/react";
import { ArrowForwardIcon } from "@chakra-ui/icons";
import PlantCard from "./PlantCard";
import { useNavigate } from 'react-router-dom';

const HomePage = () => {
  const navigate = useNavigate();
  const plants = [
    { id: 4, name: "Herbamin", price: 25, imageSrc: "herb.jpeg" },
    { id: 5, name: "Treas", price: 25, imageSrc: "treas.jpeg" },
    { id: 6, name: "Greneery", price: 25, imageSrc: "Greenery.jpeg" },
    { id: 7, name: "Monstera", price: 25, imageSrc: "Monstera_Deliciosa.jpeg" },
  ];
  
  return (
    <div className="body-container">
      <Flex>
        <Box className="column">
          <Box className="description">
            <Heading
              margin="0"
              paddingBottom={3}
              size="lg"
              fontSize="60px"
              color="teal"
            >
              Explore and Get Your Favorite Plant
            </Heading>
            <Text textAlign="justify">
              Contrary to popular belief, Lorem Ipsum is not simply random text.
              It has roots in a piece of classical Latin literature from 45 BC,
              making it over 2000 years old. Richard McClintock, a Latin
              professor at Hampden-Sydney College in Virginia, looked up one of
              the more obscure Latin words, consectetur, from a Lorem Ipsum
              passage, and going through the cites of the word in classical
              literature, discovered the undoubtable source. Lorem Ipsum comes
              from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et
              Malorum" (The Extremes of Good and Evil) by Cicero, written in 45
              BC. This book is a treatise on the theory of ethics, very popular
              during the Renaissance. The first line of Lorem Ipsum, "Lorem
              ipsum dolor sit amet..", comes from a line in section 1.10.32.
            </Text>
            <Stack marginTop="6" direction="row" spacing={4}>
              <Button colorScheme="teal" variant="solid" borderRadius={20}>
                Learn More
              </Button>
              <Button
                rightIcon={<ArrowForwardIcon />}
                colorScheme="teal"
                variant="outline"
                borderRadius={20}
              >
                Get Product
              </Button>
            </Stack>
          </Box>
        </Box>
        <Box
          className="column"
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <Box className="image">
            <Image src="garden2.png" alt="Produk Agriflora" />
          </Box>
        </Box>
      </Flex>

      <Heading
        as="h4"
        size="md"
        textAlign="center"
        paddingTop={20}
        paddingBottom={10}
      >
        Get By Categories
      </Heading>

      <Grid templateColumns="repeat(4, 1fr)" gap={4}>
        <GridItem>
          <Box textAlign="center">
            <Image
              src="indoor.jpeg"
              alt="Gambar 1"
              width="100%"
              height="auto"
            />
            <Text>Indoor Plant</Text>
          </Box>
        </GridItem>
        <GridItem>
          <Box textAlign="center">
            <Image
              src="outdoor.jpeg"
              alt="Gambar 2"
              width="100%"
              height="auto"
            />
            <Text>Outdoor Plant</Text>
          </Box>
        </GridItem>
        <GridItem>
          <Box textAlign="center">
            <Image src="herb.jpeg" alt="Gambar 3" width="100%" height="auto" />
            <Text>Herb</Text>
          </Box>
        </GridItem>
        <GridItem>
          <Box textAlign="center">
            <Image
              src="cactus.jpeg"
              alt="Gambar 4"
              width="100%"
              height="auto"
            />
            <Text>Cactus</Text>
          </Box>
        </GridItem>
      </Grid>

      <Grid templateColumns="repeat(2, 1fr)" gap={4} className="list-container">
        <Box
          textAlign="justify"
          alignItems="center"
          justifyContent="center"
          display="flex"
          flexDirection="column"
        >
          <Heading as="h4" size="md" pb={2}>
            YOU DESERVE THE BEST
          </Heading>
          <text>Get off 10% promo</text>
          <Button mt={2} colorScheme="teal" borderRadius={20}>
            Get Promo
          </Button>
        </Box>
        <Box>
          <Image src="5703606.jpg" alt="Gambar 2" width="80%" height="auto" />
        </Box>
      </Grid>

      <Flex direction="column" align="center" color="black" py={3}>
        <Box as="header">
          <Heading as="h4" size="md" textAlign="center" pt={20} pb={10}>
            Our Product
          </Heading>
        </Box>
        <Box as="nav">
          <Center>
            <Link href="#" mr={32}>
              Indoor Plant
            </Link>
            <Link href="#" mr={32}>
              Outdoor Plant
            </Link>
            <Link href="#" mr={32}>
              Seed
            </Link>
            <Link href="#">Stuf</Link>
          </Center>
        </Box>
      </Flex>

      <Flex justifyContent="space-between">
        {plants.map((plant) => (
          <PlantCard
            key={plant.id}
            name={plant.name}
            price={plant.price}
            imageSrc={plant.imageSrc}
          />
        ))}
      </Flex>
      <Center mt={6}>
        <Button 
        colorScheme="teal"
        borderRadius={20}
        onClick={() => {
              navigate('/product');
            }} >
            Load More
        </Button>
      </Center>

      <Heading as="h4" size="md" textAlign="center" paddingTop={20}>
        Join Cummunity
      </Heading>

      <Grid templateColumns="1fr 1fr" gap={4} mt={10}>
        <Box>
          <Image src="care3.png" alt="Gambar 2" width="100%" height="auto" />
        </Box>
        <Box textAlign="justify">
          <Heading as="h4" size="md" pb={2}>
            JOIN THE LARGE COMMUNITY
          </Heading>
          <Text>
            There are many variations of passages of Lorem Ipsum available, but
            the majority have suffered alteration in some form, by injected
            humour, or randomised words which don't look even slightly
            believable. If you are going to use a passage of Lorem Ipsum, you
            need to be sure there isn't anything embarrassing hidden in the
            middle of text. All the Lorem Ipsum generators on the Internet tend
            to repeat predefined chunks as necessary, making this the first true
            generator on the Internet. It uses a dictionary of over 200 Latin
            words, combined with a handful of model sentence structures, to
            generate Lorem Ipsum which looks reasonable.
          </Text>
          <Button
            rightIcon={<ArrowForwardIcon />}
            colorScheme="teal"
            variant="outline"
            mt={3}
            borderRadius={20}
          >
            Join
          </Button>
        </Box>
      </Grid>
    </div>
  );
};

export default HomePage;
