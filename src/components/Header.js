import React from 'react';
import './Header.css';
import { Box, Image, Button, Center } from '@chakra-ui/react';
import { Link } from 'react-router-dom';
import { useNavigate, useLocation } from 'react-router-dom';
import ColorModeToggle from '../components/ColorModeToggle';

function Header() {
  const navigate = useNavigate();
  const location = useLocation();
  return (
    <header className="header">
      <Image src="agriflora_logo.png" w="200px" h="auto" alt="Produk Agriflora" />
      <Box as="nav">
      <Center>
          <Link to="/" className={`nav-link ${location.pathname === '/' ? 'active' : ''}`} mr={6}>
            Home
          </Link>
          <Link to="/product" className={`nav-link ${location.pathname === '/product' ? 'active' : ''}`} mr={6}>
            Product
          </Link>
          <Link to="/about" className={`nav-link ${location.pathname === '/about' ? 'active' : ''}`} mr={6}>
            About Us
          </Link>
          <Link to="/contact" className={`nav-link ${location.pathname === '/contact' ? 'active' : ''}`} mr={6}>
            Contact
          </Link>
          <Button
            colorScheme='teal'
            size='xs'
            paddingInline={5}
            borderRadius={20}
            onClick={() => {
              navigate('/login');
            }}
          >
            Sign in
          </Button>
          <ColorModeToggle />
        </Center>
      </Box>
    </header>
  );
}

export default Header;
