import { Box, Image, Heading, Text, Flex } from "@chakra-ui/react";

function PlantCard({ imageSrc, name, price }) {
  return (
    <Box
      borderWidth="1px"
      borderRadius="md"
      overflow="hidden"
      boxShadow="lg"
      maxW="sm"
      m={2}
    >
      <Image
        src={imageSrc}
        alt={name}
        width="100%" // Menetapkan lebar gambar menjadi 100%
        height="250px" // Menetapkan tinggi gambar sesuai kebutuhan Anda
      />
      <Box p={4}>
        <Heading as="h3" size="md" mb={2}>
          {name}
        </Heading>
        <Flex justifyContent="space-between">
          <Text fontSize="lg" fontWeight="bold">
            ${price}
          </Text>
          <Box>
            {/* Add a button or any other action element here */}
          </Box>
        </Flex>
      </Box>
    </Box>
  );
}

export default PlantCard;
