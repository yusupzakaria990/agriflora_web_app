import React from 'react';
import './Category.css';
import { Link, Box, Flex, Text, Image, Button, Heading, Grid, GridItem, Center } from '@chakra-ui/react';
import PlantCard from "./PlantCard";
import { ArrowForwardIcon } from '@chakra-ui/icons';

function Category() {

    const plants = [
        { id: 4, name: "Herbamin", price: 25, imageSrc: "herb.jpeg" },
        { id: 5, name: "Treas", price: 25, imageSrc: "treas.jpeg" },
        { id: 6, name: "Greneery", price: 25, imageSrc: "Greenery.jpeg" },
        { id: 7, name: "Monstera", price: 25, imageSrc: "Monstera_Deliciosa.jpeg" },
    ];

    return (
        <div className='category-container'>

            <Heading as='h4' size='md' textAlign="center" paddingTop={20} paddingBottom={10}>
                Get By Categories
            </Heading>

            <Grid templateColumns="repeat(4, 1fr)" gap={4}>
                <GridItem>
                    <Box textAlign="center">
                        <Image
                            src="indoor.jpeg"
                            alt="Gambar 1"
                            width="100%"
                            height="auto"
                        />
                        <Text>
                            Indoor Plant
                        </Text>
                    </Box>
                </GridItem>
                <GridItem>
                    <Box textAlign="center">
                        <Image
                            src="outdoor.jpeg"
                            alt="Gambar 2"
                            width="100%"
                            height="auto"
                        />
                        <Text>
                            Outdoor Plant
                        </Text>
                    </Box>
                </GridItem>
                <GridItem>
                    <Box textAlign="center">
                        <Image
                            src="herb.jpeg"
                            alt="Gambar 3"
                            width="100%"
                            height="auto"
                        />
                        <Text>
                            Herb
                        </Text>
                    </Box>
                </GridItem>
                <GridItem>
                    <Box textAlign="center">
                        <Image
                            src="cactus.jpeg"
                            alt="Gambar 4"
                            width="100%"
                            height="auto"
                        />
                        <Text>
                            Cactus
                        </Text>
                    </Box>
                </GridItem>
            </Grid>

            <Grid
                templateColumns="repeat(2, 1fr)"
                gap={4}
                className="list-container"
            >
                <Box textAlign="justify" alignItems="center" justifyContent="center" display="flex" flexDirection="column">
                    <Heading as="h4" size="md" pb={2}>
                        YOU DESERVE THE BEST
                    </Heading>
                    <text>
                        Get off 10% promo
                    </text>
                    <Button mt={2} colorScheme='teal' borderRadius={20}>Get Promo</Button>
                </Box>
                <Box>
                    <Image
                        src="5703606.jpg"
                        alt="Gambar 2"
                        width="80%"
                        height="auto"
                    />
                </Box>
            </Grid>

            <Flex direction="column" align="center" color="black" py={3}>
                <Box as="header">
                    <Heading as="h4" size="md" textAlign="center" pt={20} pb={10}>
                        Our Product
                    </Heading>
                </Box>
                <Box as="nav">
                    <Center>
                        <Link href="#" mr={32}>
                            Indoor Plant
                        </Link>
                        <Link href="#" mr={32}>
                            Outdoor Plant
                        </Link>
                        <Link href="#" mr={32}>
                            Seed
                        </Link>
                        <Link href="#">
                            Stuf
                        </Link>
                    </Center>
                </Box>
            </Flex>

            <Flex justifyContent="space-between">
                {plants.map((plant) => (
                    <PlantCard
                        key={plant.id}
                        name={plant.name}
                        price={plant.price}
                        imageSrc={plant.imageSrc}
                    />
                ))}
            </Flex>
            <Center mt={6}>
                <Button colorScheme="teal" borderRadius={20}>Load More</Button>
            </Center>

            <Heading as='h4' size='md' textAlign="center" paddingTop={20}>
                Join Cummunity
            </Heading>

            <Grid templateColumns="1fr 1fr" gap={4} mt={10}>
                <Box>
                    <Image
                        src="care3.png"
                        alt="Gambar 2"
                        width="100%"
                        height="auto"
                    />
                </Box>
                <Box textAlign="justify">
                    <Heading as="h4" size="md" pb={2}>
                        JOIN THE LARGE COMMUNITY
                    </Heading>
                    <Text>
                        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable.
                    </Text>
                    <Button
                        rightIcon={<ArrowForwardIcon />}
                        colorScheme="teal"
                        variant="outline"
                        mt={3}
                        borderRadius={20}
                    >
                        Join
                    </Button>
                </Box>
            </Grid>

        </div>
    );
}

export default Category;